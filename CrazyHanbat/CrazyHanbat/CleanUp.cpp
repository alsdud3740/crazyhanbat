#include "stdafx.h"
#include "System.h"

extern HDC hdc;
extern HDC hMemDC;
extern HBITMAP hBit;
extern HBITMAP hOldBit;
extern BYTE *pMem;

VOID CleanUp()
{
	SelectObject(hMemDC, hOldBit);
	DeleteObject(hBit);
	DeleteDC(hMemDC);
	delete[] pMem;

	PostQuitMessage(0);
}