#include "stdafx.h"
#include "Item.h"

Item::Item() : Object()
{
}

Item::Item(INT Speed, INT Length, INT Number, const WCHAR* name, BITMAPFILE bitFile) : Object(name,bitFile)
{
	this->Speed = Speed;
	this->Length = Length;
	this->Number = Number;
	this->type = OBJTYPE::tItem;
	this->flag = TRUE;
}
INT Item:: getSpeed()
{
	return this->Speed;
}
INT Item:: getLength()
{
	return this->Length;
}
INT Item:: getNumber()
{
	return this->Number;
}
VOID Item:: OnBroke()
{
	delete this;
	printf("Item OnBroke\n");
}
VOID Item:: Onget(INT _id)
{
	this->flag = FALSE;

	Player* player = nullptr;

	for (auto& i : Resource->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}

	if (player == nullptr)
		return;

	if (this->Speed > 0)
	{
		if (player->currentSpeed != player->maxSpeed)
			player->currentSpeed += this->Speed;
	}
	else if (this->Length > 0)
	{
		if (player->currentBoomLength != player->maxBoomLength)
			player->currentBoomLength += this->Length;
	}
	else if (this->Number > 0)
	{
		if (player->currentBoomNumber != player->maxBoomNumber)
			player->currentBoomNumber += this->Number;
	}

	printf("OnGet %d\n", _id);
}

VOID Item::draw()
{
	POINT point = this->getPoint();
	GRAPHICS->AlphaBlending(&this->getBitFile(), point.x * 40 + 2, point.y * 40 + 42);
}