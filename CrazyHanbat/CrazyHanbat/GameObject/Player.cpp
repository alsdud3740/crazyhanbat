#include "stdafx.h"
#include "Player.h"

VOID Player::draw()
{
	POINT point = this->getPoint();
	
	if(this->direction == DIRECTION::Down)
		GRAPHICS->AlphaBlending(&this->getBitFile(), point.x - 30, point.y - 30);
	else if(this->direction == DIRECTION::Up)
		GRAPHICS->AlphaBlending(&this->up, point.x - 30, point.y - 30);
	else if (this->direction == DIRECTION::Left)
		GRAPHICS->AlphaBlending(&this->left, point.x - 30, point.y - 30);
	else
		GRAPHICS->AlphaBlending(&this->right, point.x - 30, point.y - 30);
}

Player::Player()
{
	this->characterName = L"Basic";
	this->type = OBJTYPE::tPlayer;
}

Player::Player(INT id) : Player()
{
	this->_id = id;

	if (this->_id == 0)
	{
		this->setPoint(20, 20);
	}
	else if (this->_id == 1)
		this->setPoint(560, 500);
}

tstring& Player::getCharacterName()
{
	return this->characterName;
}

VOID Player::OnInit()
{
	Character* temp = Resource->FindCharacter(this->characterName.c_str());

	this->currentBoomLength = temp->initBoomLength;
	this->currentBoomNumber = temp->initBoomNumber;
	this->currentSpeed = temp->initSpeed;
	this->maxBoomLength = temp->maxBoomLength;
	this->maxBoomNumber = temp->maxBoomNumber;
	this->maxSpeed = temp->maxSpeed;
	this->setBitFile(temp->bitFile);
	this->boomNumber = 0;
	this->left = temp->left;
	this->right = temp->right;
	this->up = temp->up;

	this->direction = DIRECTION::Down;
}

VOID Player::OnAttack()
{
	if (this->boomNumber == this->currentBoomNumber)
		return;

	printf("boomNumber = %d, currentBoomNumber = %d\n", this->boomNumber, this->currentBoomNumber);
	
	INT boomOfLength = this->currentBoomLength;
	POINT point = this->getPoint();

	Boom* boom = new Boom(boomOfLength, BoomTime, point.x/40, point.y/40);
	boomNumber++;
	boom->_id = this->_id;
}

VOID Player::OnDie()
{
	for (auto itr = Resource->currentGame->playerList.begin(); itr != Resource->currentGame->playerList.end();)
	{
		if (*itr == this) {
			itr = Resource->currentGame->playerList.erase(itr);
		}
		else
			itr++;
	}

	printf("Player OnDie\n");
}

VOID Player::setCharacterName(WCHAR* wstr)
{
	this->characterName.clear();
	this->characterName = wstr;
}