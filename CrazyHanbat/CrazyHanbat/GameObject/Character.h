#pragma once
#include "..\stdafx.h"

typedef struct _character
{
	tstring characterName;
	INT initBoomNumber;
	INT initBoomLength;
	INT initSpeed;
	INT maxBoomNumber;
	INT maxBoomLength;
	INT maxSpeed;
	BITMAPFILE bitFile;
	BITMAPFILE left;
	BITMAPFILE right;
	BITMAPFILE up;
}Character;
