#pragma once
#include "..\stdafx.h"
#include "Object.h"
#include "..\Util\Singleton.h"
#include "Map.h"
#include "..\Util\Type.h"
#include "Button.h"
#include "Lobby.h"
#include "Game.h"
#include "Character.h"

#define Resource ObjResource::getInstance()

class ObjResource : public Singleton<ObjResource>
{
private:
	std::unordered_map<tstring, Object*> gameObject;
	std::unordered_map<tstring, MAP*> Map;
	std::unordered_map<tstring, Character*> character;
	std::unordered_map<tstring, BITMAPFILE*> etcBit;
	std::unordered_map<tstring, MYBRUSH*> brush;

public:
	Lobby* currentLobby;
	Game* currentGame;
	std::list<Button*> button;

public:
	ObjResource();
	BOOL Clear();

	BOOL Insert(const WCHAR* wstr, Object* obj);
	Object* FindObj(const WCHAR* wstr);
	BOOL Insert(const WCHAR* wstr, MAP* obj);
	MAP* FindMap(const WCHAR* wstr);
	unordered_map<tstring, MAP*>* getMap();
	BOOL Insert(const WCHAR* wstr, BITMAPFILE* obj);
	BITMAPFILE* FindBit(const WCHAR* wstr);
	BOOL Insert(Button* button);
	list<Button*>* getButton();
	BOOL Insert(const WCHAR* wstr, Character* obj);
	Character* FindCharacter(const WCHAR* wstr);
	BOOL Insert(const WCHAR* wstr, MYBRUSH* obj);
	MYBRUSH* FindBrush(const WCHAR* wstr);
};