#include "stdafx.h"
#include "Lobby.h"

Lobby::Lobby()
{
	this->bitFile = *Resource->FindBit(L"Lobby");

	Button *temp = new Button(510, 495, 195, 48, *Resource->FindBit(L"StartButton"), [] 
		{
			for (auto itr = Resource->button.begin(); itr != Resource->button.end();)
			{
				delete *itr;
				itr++;
			}

			Resource->currentGame = new Game();

			if (Resource->currentGame == nullptr)
			{
				Resource->Clear();
				delete Resource->currentLobby;
				exit(-1);
			}

			delete Resource->currentLobby;
			SCENEMANAGER->GoNext();
		}
	);
	Button *temp2 = new Button(487, 64, 90, 59, *Resource->FindBit(L"SelectBase"), [] {
	
		for (auto &i : Resource->currentLobby->playerList)
		{
			if (i->_id == 0)
				i->setCharacterName(L"Basic");
		}
	});
	Button *temp3 = new Button(488, 355, 54, 114, *Resource->FindBit(L"ArrowLeft"), [] {});
	Button *temp4 = new Button(717, 355, 54, 114, *Resource->FindBit(L"ArrowRight"), [] {});
	Button *temp5 = new Button(587, 64, 90, 59, *Resource->FindBit(L"SelectSecond"), [] {
	
		for (auto &i : Resource->currentLobby->playerList)
		{
			if (i->_id == 0)
				i->setCharacterName(L"Second");
		}

	});

	INT i = 0;

	Player* player = new Player(i);
	Player* player2 = new Player(++i);

	auto iter = Resource->getMap()->begin();
	this->mapName = iter->first.c_str();

	this->playerList.push_back(player);
	this->playerList.push_back(player2);
}

VOID Lobby::Draw()
{
	GRAPHICS->DrawBitFile(&this->bitFile, 0, 0);

	for (auto &i : Resource->button)
	{
		i->draw();
	}

	for (auto &i : this->playerList)
	{
		Character *temp = Resource->FindCharacter(i->getCharacterName().c_str());
		if (i->_id == 0)
		{
			GRAPHICS->AlphaBlending(&temp->bitFile, 42, 105);
		}
		else
			GRAPHICS->AlphaBlending(&temp->bitFile, 152, 105);
	}
	
	GRAPHICS->Finish();
}

Lobby::~Lobby()
{
	
}

const tstring& Lobby::getName()
{
	return this->mapName;
}