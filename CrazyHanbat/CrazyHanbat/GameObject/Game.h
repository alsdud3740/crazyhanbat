#pragma once
#include "..\stdafx.h"
#include "Player.h"
#include "Object.h"
#include "..\Util\Graphics.h"

class Game
{
public:
	Game();
	list<Player*> playerList;
	array<list<Object*>, 15 * 13> GameObject;
	BITMAPFILE mapBitFile;
	INT time;
	VOID draw();
	VOID Clean();
};