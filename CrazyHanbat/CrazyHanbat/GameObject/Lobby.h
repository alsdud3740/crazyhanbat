#pragma once
#include "..\stdafx.h"
#include "Button.h"

class Lobby
{
public:
	list<Player*> playerList;
	tstring mapName;
	BITMAPFILE bitFile;

public:
	Lobby();
	~Lobby();
	VOID Draw();
	const tstring& getName();
};