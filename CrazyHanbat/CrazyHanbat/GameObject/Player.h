#pragma once
#include "..\stdafx.h"
#include "Object.h"
#include "Boom.h"

#define BoomTime 3000

typedef enum : INT
{
	Up = 0,
	Down,
	Left,
	Right,
}DIRECTION;

class Player : public Object
{
private:
	INT maxBoomLength;
	INT currentBoomLength;
	INT maxBoomNumber;
	INT currentBoomNumber;
	INT maxSpeed;
	INT currentSpeed;
	tstring characterName;
	BITMAPFILE left;
	BITMAPFILE right;
	BITMAPFILE up;
	
public:
	INT boomNumber = 0;
	DIRECTION direction;

public:
	INT _id;

public:
	friend class Item;

public:
	VOID OnInit();
	VOID OnAttack();
	Player();
	Player(INT id);
	VOID draw();
	tstring& getCharacterName();
	VOID setCharacterName(WCHAR* wstr);
	VOID OnDie();
};