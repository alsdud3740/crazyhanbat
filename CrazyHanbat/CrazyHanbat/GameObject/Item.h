#pragma once
#include "..\stdafx.h"
#include "Object.h"
	
class Item : public Object 
{
public:
		INT Speed;
		INT Length;
		INT Number;
		INT flag;

public:
	Item();
	Item(INT isSpeed, INT isLength, INT isNumber, const WCHAR* name, BITMAPFILE bitFile);
	VOID OnBroke();
	VOID Onget(INT _id);
	INT getSpeed();
	INT getLength();
	INT getNumber();
	VOID draw();
	
};
