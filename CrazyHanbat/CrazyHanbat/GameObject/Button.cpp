#include "stdafx.h"
#include "Button.h"

Button::Button()
{

}

Button::Button(INT x, INT y,INT width, INT height,BITMAPFILE bitFile, function<VOID()> func)
{
	this->x = x;
	this->y = y;
	this->height = height;
	this->width = width;
	this->OnClick = func;
	this->bitFile = bitFile;

	Resource->Insert(this);
}

Button::~Button()
{
	
}

VOID Button::draw()
{
	GRAPHICS->DrawBitFile(&this->bitFile, this->x, this->y);
}