#pragma once
#include "..\stdafx.h"
#include "..\Util\Singleton.h"
#include "..\Util\Type.h"
#include "..\Util\Graphics.h"
#include <list>

#define EFFECTMANAGER EffectPool::getInstance()
#define EFFECTTIME 1000

typedef enum : INT
{
	width = 0,
	height,
	center,
}EFFECTTYPE;

class Effect : public MemoryPool<Effect>
{
public:
	INT x, y;
	BITMAPFILE width;
	BITMAPFILE height;
	BITMAPFILE center;
	EFFECTTYPE type;
	INT TIME;

	Effect()
	{
		width = *Resource->FindBit(L"EffectLeft");
		height = *Resource->FindBit(L"EffectDown");
		center = *Resource->FindBit(L"EffectCenter");
	}
	Effect(EFFECTTYPE type, INT X, INT Y, INT time) : Effect()
	{
		this->type = type;
		this->x = X;
		this->y = Y;
		this->TIME = time;
	}
};

class EffectPool : public Singleton<EffectPool>
{
private:
	list<Effect*> effectList;

public:
	VOID EffectWidth(INT X, INT Y, INT TIME)
	{
		printf("EffectWidth = %d, %d, %d\n", X, Y, TIME);
		Effect *newefc = new Effect(EFFECTTYPE::width, X, Y, TIME);
		this->effectList.emplace_back(newefc);
	}

	VOID EffectHeight(INT X, INT Y, INT TIME)
	{
		printf("EffectHeight = %d %d %d\n", X, Y, TIME);
		Effect *newefc = new Effect(EFFECTTYPE::height, X, Y, TIME);
		this->effectList.emplace_back(newefc);
	}

	VOID EffectCenter(INT X, INT Y, INT TIME)
	{
		printf("EffectCenter = %d %d %d\n", X, Y, TIME);
		Effect *newefc = new Effect(EFFECTTYPE::center, X, Y, TIME);
		this->effectList.emplace_back(newefc);
	}

	VOID Draw()
	{
		for (auto itr = this->effectList.begin(); itr != this->effectList.end();)
		{
			INT X = reinterpret_cast<Effect*>(*itr)->x;
			INT Y = reinterpret_cast<Effect*>(*itr)->y;

			if (reinterpret_cast<Effect*>(*itr)->type == EFFECTTYPE::height)
			{
				GRAPHICS->DrawBitFile(&reinterpret_cast<Effect*>(*itr)->height, X * 40 + 2, Y * 40 + 42);
			}
			else if (reinterpret_cast<Effect*>(*itr)->type == EFFECTTYPE::width)
				GRAPHICS->DrawBitFile(&reinterpret_cast<Effect*>(*itr)->width, X * 40 + 2, Y * 40 + 42);
			else
				GRAPHICS->DrawBitFile(&reinterpret_cast<Effect*>(*itr)->center, X * 40 + 2, Y * 40 + 42);

			itr++;
		}
	}

	VOID CheckEffect()
	{
		for (auto itr = this->effectList.begin(); itr != this->effectList.end();)
		{
			reinterpret_cast<Effect*>(*itr)->TIME -= EVENTMANAGER->deltaTime().count();

			if (reinterpret_cast<Effect*>(*itr)->TIME < 0)
				itr = this->effectList.erase(itr);
			else
				itr++;
		}
	}


};