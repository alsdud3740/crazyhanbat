#include "stdafx.h"
#include "ObjResource.h"

ObjResource::ObjResource()
{

}

BOOL ObjResource::Clear()
{
	for (auto &i : this->brush)
	{
		if (i.second != nullptr)
			delete i.second;
	}

	for (auto &i : this->character)
	{
		if (i.second != nullptr)
			delete i.second;
	}

	for (auto &i : this->gameObject)
	{
		if (i.second != nullptr)
			delete i.second;
	}

	for (auto &i : this->etcBit)
	{
		if (i.second != nullptr)
			delete i.second;
	}

	return TRUE;
}

BOOL ObjResource::Insert(const WCHAR* wstr, Object* obj)
{
	tstring str = wstr;

	this->gameObject.insert(std::unordered_map<tstring, Object*>::value_type(str, obj));

	return TRUE;
}

Object* ObjResource::FindObj(const WCHAR* wstr)
{
	tstring str = wstr;

	auto found = this->gameObject.find(str);

	if (found != gameObject.end())
		return found->second;
	else
		return 0;
}

BOOL ObjResource::Insert(const WCHAR* wstr, MAP* obj)
{
	tstring str = wstr;

	this->Map.insert(std::unordered_map<tstring, MAP*>::value_type(str, obj));

	return TRUE;
}

MAP* ObjResource::FindMap(const WCHAR* wstr)
{
	tstring str = wstr;

	auto found = this->Map.find(str);

	if (found != Map.end())
		return found->second;
	else
		return 0;
}

BOOL ObjResource::Insert(const WCHAR* wstr, BITMAPFILE* obj)
{
	tstring str = wstr;

	this->etcBit.insert(std::unordered_map<tstring, BITMAPFILE*>::value_type(str, obj));

	return TRUE;
}

BITMAPFILE* ObjResource::FindBit(const WCHAR* wstr)
{
	tstring str = wstr;

	auto found = this->etcBit.find(str);

	if (found != etcBit.end())
		return found->second;
	else
		return 0;
}

BOOL ObjResource::Insert(Button* button)
{
	this->button.emplace_back(button);

	return TRUE;
}

BOOL ObjResource::Insert(const WCHAR* wstr, Character* obj)
{
	tstring str = wstr;

	this->character.insert(std::unordered_map<tstring, Character*>::value_type(str, obj));

	return TRUE;
}

Character* ObjResource::FindCharacter(const WCHAR* wstr)
{
	tstring str = wstr;

	auto found = this->character.find(str);

	if (found != character.end())
		return found->second;
	else
		return 0;
}

unordered_map<tstring, MAP*>* ObjResource::getMap()
{
	return &this->Map;
}

BOOL ObjResource::Insert(const WCHAR* wstr, MYBRUSH* obj)
{
	tstring str = wstr;
	
	this->brush.insert(std::unordered_map<tstring, MYBRUSH*>::value_type(str, obj));

	return TRUE;
}

MYBRUSH* ObjResource::FindBrush(const WCHAR* wstr)
{
	tstring str = wstr;

	auto found = this->brush.find(str);

	if (found != brush.end())
		return found->second;
	else
		return 0;
}