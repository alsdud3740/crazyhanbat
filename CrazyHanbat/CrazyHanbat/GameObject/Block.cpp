#include "stdafx.h"
#include "Block.h"

Block::Block() : Object()
{
}

Block::Block(BOOL isWall, BOOL isPass,const WCHAR* name ,BITMAPFILE bitFile) : Object(name, bitFile)
{
	this->isWall = isWall;
	this->isPass = isPass;
	this->type = OBJTYPE::tBlock;
}

BOOL Block::pass()
{
	return this->isPass;
}

BOOL Block::wall()
{
	return this->isWall;
}

VOID Block::OnBroke()
{
	POINT point = this->getPoint();
	Item *newitem = 0;

	switch (RANDOM->getRandom(3, FALSE))
	{
	case 0:
		newitem = new Item(*reinterpret_cast<Item*>(Resource->FindObj(L"BoomItem")));
		newitem->setPoint(point.x, point.y);
		Resource->currentGame->GameObject[point.y * 15 + point.x].emplace_front(newitem);
		break;
	case 1:
		newitem = new Item(*reinterpret_cast<Item*>(Resource->FindObj(L"PowerItem")));
		newitem->setPoint(point.x, point.y);
		Resource->currentGame->GameObject[point.y * 15 + point.x].emplace_front(newitem);
		break;
	case 2:
		break;
	}

	delete this;
	printf("Block OnBroke\n");
}

VOID Block::draw()
{
	POINT point = this->getPoint();
	GRAPHICS->DrawBitFile(&this->getBitFile(), point.x * 40 + 2, point.y * 40 + 42);
}