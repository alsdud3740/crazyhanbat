#pragma once
#include "..\stdafx.h"
#include "..\Util\MemoryPool.h"

class Boom : public Object, public MemoryPool<Boom>
{
private:
	INT boomOfLength;
	INT time;
	BOOL isActive;

public:
	INT _id;

public:
	Boom();
	Boom(INT boomOfLength, INT time, INT x, INT y);
	~Boom();
	VOID OnBurst();
	VOID draw();
	BOOL OnTime(INT tick);
};