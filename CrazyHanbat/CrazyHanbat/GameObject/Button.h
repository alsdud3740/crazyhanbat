#pragma once
#include "..\stdafx.h"

class Button
{
public:
	BITMAPFILE bitFile;
	INT x, y;
	INT width, height;
	Button();
	Button(INT x, INT y,INT width, INT height, BITMAPFILE bitFile, function<VOID()> func);
	~Button();
	VOID draw();
	function<VOID()> OnClick;
};