#pragma once
#include "..\stdafx.h"

typedef enum : UINT
{
	tObject = 0,
	tPlayer,
	tBoom,
	tItem,
	tBlock,
}OBJTYPE;

class Object
{
private:
	tstring name;
	POINT point;
	BITMAPFILE bitFile;

public:
	OBJTYPE type;

public:
	Object();
	Object(const WCHAR* name, BITMAPFILE bitFile);
	virtual VOID draw() = 0;
	VOID setPoint(int x, int y);
	VOID setBitFile(BITMAPFILE& p_bitFile);
	POINT& getPoint();
	BITMAPFILE& getBitFile();
	tstring& getName();
};