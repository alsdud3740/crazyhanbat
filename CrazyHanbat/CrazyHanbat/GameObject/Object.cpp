#include "stdafx.h"
#include "Object.h"

Object::Object()
{
	this->name.clear();
	this->point.x = 0;
	this->point.y = 0;
	this->type = OBJTYPE::tObject;
}

Object::Object(const WCHAR* name, BITMAPFILE bitFile) : Object()
{
	this->name = name;
	this->bitFile = bitFile;
}

VOID Object::setPoint(int x, int y)
{
	this->point.x = x;
	this->point.y = y;
}

VOID Object::setBitFile(BITMAPFILE& p_bitFile)
{
	this->bitFile = p_bitFile;
}

POINT& Object::getPoint()
{
	return this->point;
}

BITMAPFILE& Object::getBitFile()
{
	return this->bitFile;
}

tstring& Object::getName()
{
	return this->name;
}