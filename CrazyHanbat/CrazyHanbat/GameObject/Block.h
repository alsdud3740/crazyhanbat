#pragma once
#include "../stdafx.h"
#include "Object.h"

class Block : public Object
{
private:
	BOOL isWall;
	BOOL isPass;
public:
	Block();
	Block(BOOL isWall, BOOL isPass,const WCHAR* name, BITMAPFILE bitFile);
	BOOL wall();
	BOOL pass();
	VOID OnBroke();
	VOID draw();
};