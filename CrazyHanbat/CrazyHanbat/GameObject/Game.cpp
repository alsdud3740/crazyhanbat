#include "stdafx.h"
#include "Game.h"

Game::Game()
{
	auto iter = this->playerList.begin();
	this->playerList.insert(iter,Resource->currentLobby->playerList.begin(),Resource->currentLobby->playerList.end());

	MAP* currentMap = Resource->FindMap(Resource->currentLobby->mapName.c_str());

	this->mapBitFile = currentMap->mapBitFile;
	this->time = 270;
	
	for (auto& i : currentMap->blockList)
	{
		POINT point = i->getPoint();
		Block* block = new Block(*i);
		
		this->GameObject[point.y * 15 + point.x].push_back(block);
	}

	for (auto& i : this->playerList)
	{
		i->OnInit();
		POINT point = i->getPoint();
		this->GameObject[(point.y / 40) * 15 + (point.x/40)].push_back(i);
	}

	PLAYERMANAGER->Set(this);
}

VOID Game::draw()
{
	GRAPHICS->DrawRectangle(0, 0, 800, 600, Resource->FindBrush(L"GameBC"));
	GRAPHICS->DrawBitFile(&this->mapBitFile, 0, 40);
	
	for (auto &i : this->GameObject)
	{
		for (auto &a : i)
		{
			a->draw();
		}
	}

	EFFECTMANAGER->Draw();

	GRAPHICS->Finish();
}

VOID Game::Clean()
{
	
}