#pragma once
#include "Block.h"

typedef struct _map{
	wstring mapName;
	forward_list<Block*> blockList;
	BITMAPFILE mapBitFile;
}MAP;