#include "stdafx.h"
#include "Boom.h"

Boom::Boom() : Object(L"BOOM", *Resource->FindBit(L"Boom1"))
{
	this->isActive = FALSE;
	this->type = OBJTYPE::tBoom;
}

Boom::Boom(INT boomOfLength, INT time, INT x, INT y) : Object(L"BOOM", *Resource->FindBit(L"Boom1"))
{
	this->boomOfLength = boomOfLength;
	this->time = time;
	this->type = OBJTYPE::tBoom;
	this->setPoint(x, y);
	this->isActive = TRUE;

	Resource->currentGame->GameObject[y * 15 + x].emplace_front(this);
	EVENTMANAGER->boomList.emplace_front(this);
}

Boom::~Boom()
{
	this->isActive = FALSE;
}

VOID Boom::OnBurst()
{
	if (this->isActive == FALSE)
		return;

	POINT point = this->getPoint();
	this->isActive = FALSE;
	BOOL flag = TRUE;

	Resource->currentGame->GameObject[point.y * 15 + point.x].remove(this);

	// 가운데 검사
	for (auto itr = Resource->currentGame->GameObject[point.y * 15 + point.x].begin(); itr != Resource->currentGame->GameObject[point.y * 15 + point.x].end();)
	{
		if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBlock)
		{
			reinterpret_cast<Block*>(*itr)->OnBroke();
			itr = Resource->currentGame->GameObject[point.y * 15 + point.x].erase(itr);
			break;
		}
		else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer)
			reinterpret_cast<Player*>(*itr)->OnDie();
		else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			reinterpret_cast<Item*>(*itr)->OnBroke();
		else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBoom)
		{
			if (reinterpret_cast<Boom*>(*itr)->isActive == TRUE)
			{
				reinterpret_cast<Boom*>(*itr)->time = 0;
			}
		}
		itr++;
	}

	EFFECTMANAGER->EffectCenter(point.x, point.y, EFFECTTIME);

	// 위쪽 검사
	for (INT i = 1; i < this->boomOfLength; i++)
	{
		if (point.y - i < 0)
			continue;

		if (flag == FALSE)
			break;

		EFFECTMANAGER->EffectHeight(point.x, point.y - i, EFFECTTIME);

		if (Resource->currentGame->GameObject[(point.y - i) * 15 + point.x].empty())
			continue;

		for (auto itr = Resource->currentGame->GameObject[(point.y - i) * 15 + point.x].begin(); itr != Resource->currentGame->GameObject[(point.y - i) * 15 + point.x].end();)
		{
			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBlock)
			{
				reinterpret_cast<Block*>(*itr)->OnBroke();
				itr = Resource->currentGame->GameObject[(point.y - i) * 15 + point.x].erase(itr);
				flag = FALSE;
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer)
				reinterpret_cast<Player*>(*itr)->OnDie();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
				reinterpret_cast<Item*>(*itr)->OnBroke();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBoom)
			{
				if (reinterpret_cast<Boom*>(*itr)->isActive == TRUE)
				{
					reinterpret_cast<Boom*>(*itr)->time = 0;
				}
			}

			itr++;
		}

		
	}

	flag = TRUE;

	// 아래쪽 검사
	for (INT i = 1; i < this->boomOfLength; i++)
	{
		if (point.y + i > 12)
			continue;

		if (flag == FALSE)
			break;

		EFFECTMANAGER->EffectHeight(point.x, point.y + i, EFFECTTIME);

		if (Resource->currentGame->GameObject[(point.y + i) * 15 + point.x].empty())
			continue;
		
		for (auto itr = Resource->currentGame->GameObject[(point.y + i) * 15 + point.x].begin(); itr != Resource->currentGame->GameObject[(point.y + i) * 15 + point.x].end();)
		{
			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBlock)
			{
				reinterpret_cast<Block*>(*itr)->OnBroke();
				itr = Resource->currentGame->GameObject[(point.y + i) * 15 + point.x].erase(itr);
				flag = FALSE;
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer)
				reinterpret_cast<Player*>(*itr)->OnDie();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
				reinterpret_cast<Item*>(*itr)->OnBroke();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBoom)
			{
				if (reinterpret_cast<Boom*>(*itr)->isActive == TRUE)
				{
					reinterpret_cast<Boom*>(*itr)->time = 0;
				}
			}

			itr++;
		}
		
	}

	flag = TRUE;

	// 오른쪽 검사
	for (INT i = 1; i < this->boomOfLength; i++)
	{
		if (point.x + i > 14)
			continue;

		if (flag == FALSE)
			break;

		EFFECTMANAGER->EffectWidth(point.x + i, point.y, EFFECTTIME);

		if (Resource->currentGame->GameObject[point.y * 15 + (point.x + i)].empty())
			continue;

		for (auto& itr = Resource->currentGame->GameObject[point.y * 15 + (point.x + i)].begin(); itr != Resource->currentGame->GameObject[point.y * 15 + (point.x + i)].end();)
		{
			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBlock)
			{
				reinterpret_cast<Block*>(*itr)->OnBroke();
				itr = Resource->currentGame->GameObject[point.y * 15 + (point.x + i)].erase(itr);
				flag = FALSE;
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer)
				reinterpret_cast<Player*>(*itr)->OnDie();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
				reinterpret_cast<Item*>(*itr)->OnBroke();
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBoom)
			{
				if (reinterpret_cast<Boom*>(*itr)->isActive == TRUE)
				{
					reinterpret_cast<Boom*>(*itr)->time = 0;
				}
			}

			itr++;
		}

		
	}

	flag = TRUE;

	// 왼쪽 검사
	for (INT i = 1; i < this->boomOfLength; i++)
	{
		if (point.x - i < 0)
			continue;

		if (flag == FALSE)
			break;

		EFFECTMANAGER->EffectWidth(point.x - i, point.y, EFFECTTIME);

		if (Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].empty())
			continue;

		for (auto& itr = Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].begin(); itr != Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].end();)
		{
			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBlock)
			{
				reinterpret_cast<Block*>(*itr)->OnBroke();
				itr = Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].erase(itr);
				flag = FALSE;
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer)
			{
				reinterpret_cast<Player*>(*itr)->OnDie();
				itr = Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].erase(itr);
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			{
				reinterpret_cast<Item*>(*itr)->OnBroke();
				itr = Resource->currentGame->GameObject[point.y * 15 + (point.x - i)].erase(itr);
				continue;
			}
			else if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tBoom)
			{
				if (reinterpret_cast<Boom*>(*itr)->isActive == TRUE)
				{
					reinterpret_cast<Boom*>(*itr)->time = 0;
				}
			}
			itr++;
		}

		
	}

	for (auto& i : Resource->currentGame->playerList)
	{
		if (i->_id == this->_id)
			i->boomNumber--;
	}

	delete this;
}

VOID Boom::draw()
{
	POINT point = this->getPoint();
	GRAPHICS->AlphaBlending(&this->getBitFile(), point.x * 40 + 2, point.y * 40 + 42);
}

BOOL Boom::OnTime(INT tick)
{
	time -= tick;

	if (time <= 0)
		return TRUE;
	else
		return FALSE;
}