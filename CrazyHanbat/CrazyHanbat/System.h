#pragma once

#ifdef _DEBUG
#pragma comment(linker, "/entry:WinMainCRTStartup")
#pragma comment(linker, "/subsystem:console")
#endif

#include "stdafx.h"
#include <Windows.h>
#include <tchar.h>

#define LOADMAXSTRING 100
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// 메세지 처리
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
VOID ProcessInput(HWND hWnd, WPARAM keyPress);

// 게임 Input 처리
VOID ProcessInGameInput();

// 게임 루프 관련
VOID Update(HWND hWnd);

// Init
VOID Init(HWND hWnd);

// 뒷정리 관련
VOID CleanUp();

// 버튼 관련
VOID CheckButton(HWND hWnd, INT x, INT y);

// 진입점 함수
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, INT);

// 문자열 함수
INT MyAnsiToInt(CHAR ansi);
VOID MyAnsiToUnicode(CHAR* ansi, std::wstring* out);
BOOL MyIntToAnsi(INT a, CHAR* buffer);