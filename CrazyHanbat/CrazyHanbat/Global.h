#pragma once

#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS 1

#include <array>
#include <string>
#include <Windows.h>
#include <tchar.h>
#include <chrono>
#include <ctime>
#include <functional>
#include <iostream>
#include <random>
#include <list>
#include <vector>
#include <crtdbg.h>
#include <hash_map>
#include <forward_list>
#include <unordered_map>
#include <memory>

// System 필수 헤더
#include "System.h"

// Util 필수 헤더
#include "./Util/Singleton.h"
#include "./Util/Type.h"
#include "./Util/Clock.h"
#include "./Util/Random.h"
#include "./Util/MemoryPool.h"
#include "./Util/LowFragmentationHeap.h"
#include "./Util/Minidumb.h"
#include "./Util/Graphics.h"

// GameObject 필수 헤더
#include "GameObject\Object.h"
#include "GameObject\Block.h"
#include "GameObject\Player.h"
#include "GameObject\Boom.h"
#include "GameObject\Character.h"
#include "GameObject\Game.h"
#include "GameObject\Item.h"
#include "GameObject\Lobby.h"
#include "GameObject\Map.h"
#include "GameObject\Button.h"
#include "GameObject\ObjResource.h"


// GameLogic 필수 헤더
#include "./Logic/Scene.h"
#include "./Logic/GameEventManager.h"
#include "./Logic/PlayerManager.h"
#include "GameObject\EffectPool.h"

// 메크로
#define snprintf(dst, format, ...)     _snprintf_s(dst.data(), dst.size(), _TRUNCATE, format, __VA_ARGS__)
#define snwprintf(dst, format, ...)    _snwprintf_s(dst.data(), dst.size(), _TRUNCATE, format, __VA_ARGS__)