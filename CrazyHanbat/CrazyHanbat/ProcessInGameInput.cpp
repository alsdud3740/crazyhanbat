#include "stdafx.h"
#include "System.h"

VOID ProcessInGameInput()
{
	if (SCENEMANAGER->GetCurrentScene() == SCENE::GAME)
	{
		if (GetAsyncKeyState(VK_LEFT) == 0xffff8000)
		{
			PLAYERMANAGER->Left(0);
		}

		if (GetAsyncKeyState(VK_RIGHT) == 0xffff8000)
		{
			PLAYERMANAGER->Right(0);
		}

		if (GetAsyncKeyState(VK_UP) == 0xffff8000)
		{
			PLAYERMANAGER->Up(0);
		}

		if (GetAsyncKeyState(VK_DOWN) == 0xffff8000)
		{
			PLAYERMANAGER->Down(0);
		}

		if (GetAsyncKeyState(VK_SPACE) == 0xffff8000)
		{
			PLAYERMANAGER->OnAttack(0);
		}

		if (GetAsyncKeyState(0x44) == 0xffff8000)
		{
			PLAYERMANAGER->Right(1);
		}

		if (GetAsyncKeyState(0x57) == 0xffff8000)
		{
			PLAYERMANAGER->Up(1);
		}

		if (GetAsyncKeyState(0x41) == 0xffff8000)
		{
			PLAYERMANAGER->Left(1);
		}

		if (GetAsyncKeyState(0x53) == 0xffff8000)
		{
			PLAYERMANAGER->Down(1);
		}

		if (GetAsyncKeyState(VK_SHIFT) == 0xffff8000)
		{
			PLAYERMANAGER->OnAttack(1);
		}
	}
}