#include "stdafx.h"
#include "System.h"

extern HDC hdc;
extern HDC hMemDC;

// �Լ� ���
void Draw(HWND hWnd);

// Main Function -------------------------------------
void Update(HWND hWnd)
{
	static timePoint oldTime = NOW_TIMEPOINT;
	static timePoint newTime = NOW_TIMEPOINT;
	static UINT64 iCount = 0;

	std::array<CHAR, SIZE_16> countStr;
	static string timeStr = { 0, };

	seconds second;

	// TODO :
	
	Draw(hWnd);

	// END

	newTime = NOW_TIMEPOINT;
	second = duration_cast<seconds>(newTime - oldTime);
	iCount++;

	if (second.count() >= 1.0f) {
		snprintf(countStr, "%d", (UINT)(iCount));
		timeStr = countStr.data();
		oldTime = NOW_TIMEPOINT;
		iCount = 0;
	}

	TextOutA(hMemDC, 0, 0, timeStr.c_str(), strlen(timeStr.c_str()));
	BitBlt(hdc, 0, 0, WIN_WIDTH, WIN_HEIGHT, hMemDC, 0, 0, SRCCOPY);
}

// -------------------------------------------------------------
void Draw(HWND hWnd)
{
	SCENE scene;

	switch (scene = SCENEMANAGER->GetCurrentScene())
	{
	case SCENE::Title:
		// TODO:
		GRAPHICS->DrawBitFile(Resource->FindBit(L"Title"), 0, 0);
		GRAPHICS->Finish();
		break;
	case SCENE::LOBBY:
		Resource->currentLobby->Draw();
		// TODO:
		break;
	case SCENE::GAME:
		Resource->currentGame->draw();
		// TODO:
		break;
	}
}