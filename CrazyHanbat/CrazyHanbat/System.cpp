#include "stdafx.h"
#include "System.h"

// 전역 변수
WCHAR gAppName[LOADMAXSTRING] = L"프레임 워크";
HINSTANCE gInst;

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, INT)
{
	gInst = hInst;

	WNDCLASSEX wcex;
	memset(&wcex, 0, sizeof(WNDCLASSEX));

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = MsgProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszClassName = gAppName;
	wcex.hIcon = LoadIcon(nullptr, IDC_ICON);
	wcex.hIconSm = wcex.hIcon;

	RegisterClassExW(&wcex);

	DWORD style = WS_CAPTION | WS_SYSMENU;
	HWND hWnd = CreateWindow(gAppName, gAppName,
		style, CW_USEDEFAULT, 0, WIN_WIDTH, WIN_HEIGHT,
		GetDesktopWindow(), NULL, wcex.hInstance, NULL);

	POINT ptDiff;
	RECT rcClient, rcWindow;

	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWindow);
	ptDiff.x = (rcWindow.right - rcWindow.left) - rcClient.right;
	ptDiff.y = (rcWindow.bottom - rcWindow.top) - rcClient.bottom;
	MoveWindow(hWnd, rcWindow.left, rcWindow.top, WIN_WIDTH + ptDiff.x, WIN_HEIGHT + ptDiff.y, TRUE);

	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	while (msg.message != WM_QUIT)
	{
		EVENTMANAGER->CheckTime();
		EVENTMANAGER->EventCheck();
		EFFECTMANAGER->CheckEffect();
		EVENTMANAGER->GameCheck();

		ProcessInGameInput();

		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE) && msg.message != WM_PAINT)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Update(hWnd);
		}
	}

	return 0;
}