#include "stdafx.h"
#include "System.h"

LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
		Init(hWnd);
		break;
	case WM_LBUTTONDOWN:
		CheckButton(hWnd, LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		ProcessInput(hWnd, wParam);
		break;
	case WM_DESTROY:
		CleanUp();
		break;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return 0;
}