#pragma once
#include "..\stdafx.h"

// 타입 정의
typedef std::wstring tstring;
typedef std::time_t tick_t;
typedef std::chrono::system_clock::time_point timePoint;

#pragma pack(push, 1)

typedef struct _BITFILEHEADER
{
	WORD bfType;				// BM 이라는 캐릭터형 저장
	DWORD bfSize;				// 파일의 전체 크기
	WORD reserved1;				// 예약 (사용 X)
	WORD reserved2;				// 예약
	DWORD bfOffBits;			// 실질 데이터(Pixel)의 시작 좌표 (파일 정보, 이미지 정보, 색상 테이블 구조체의 크기를 더한 값)
}BITFILEHEADER;
typedef struct _BITFILEINFOHEADER
{
	DWORD biSize;				// 구조체 크기
	DWORD biWidth;				// 이미지 폭
	DWORD biHeight;				// 이미지 높이
	WORD biPlanes;				// 현재 지원값 1
	WORD biBitCount;			// 픽셀 당 비트수 1,4,8,16,24,32
	DWORD biCompression;		// 압축 코드
	LONG biSizeImage;			// 이미지에 들어 있는 바이트 수
	LONG biXPelsPerMeter;		// 미터당 픽셀수 x축
	LONG biYPelsPerMeter;		// 미터당 픽셀수 y축
	DWORD biClrUsed;			// 미터당 실질적으로 사용될 컬러맵의 엔트리수
	DWORD biClrImportant;		// 주로 사용되는 컬러수
}BITFILEINFOHEADER;
typedef struct _BITPALETTE
{
	BYTE blue;
	BYTE green;
	BYTE red;
	BYTE reserved;
}PALLETTE;
typedef struct _BITMAPFILE
{
	// FILE HEADER
	BITFILEHEADER bitFileHeader;

	// IMAGE HEADER
	BITFILEINFOHEADER bitFileInfoHeader;

	// RGB TABLE
	PALLETTE bitPallette[126];

	// 이미지 데이터
	BYTE *buffer;

}BITMAPFILE;

#pragma pack(pop)

typedef struct _MyBrush
{
	BYTE DATA[WIN_WIDTH * 4];
}MYBRUSH;

// 크기 정의
#define SIZE_8		8
#define SIZE_16		16
#define SIZE_32		32
#define SIZE_64		64
#define SIZE_128	128
#define SIZE_256	256
#define SIZE_1024	1024
#define SIZE_2048	2048
#define SIZE_4096	4096
#define SIZE_8192	8192