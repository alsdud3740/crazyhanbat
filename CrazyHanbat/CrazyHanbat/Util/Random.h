#pragma once
#include "..\stdafx.h"
#include <random>
#include <chrono>

#define RANDOM							Random::getInstance()
#define GETRANDOM(maxVal, isDevice)		RANDOM->getRandom(maxVal, isDevice)

class Random : public Singleton<Random>
{
public:
	UINT64 getRandom(INT maxVal, BOOL isDevice)
	{
		if (!isDevice)
		{
			auto curTime = CLOCK->nowTimePoint();
			auto duration = curTime.time_since_epoch();
			auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
			std::mt19937_64 engine(millis);
			std::uniform_int_distribution<UINT64> dist1(0, UINT64_MAX);
			auto func = bind(dist1, engine);

			return static_cast<UINT64>(func() % maxVal);
		}
		else
		{
			std::random_device rng;
			auto seed = rng();
			std::mt19937_64 engine(seed);
			std::uniform_int_distribution<UINT64> dist1(0, UINT64_MAX);
			auto func = bind(dist1, engine);

			return static_cast<UINT64>(func() % maxVal);
		}
	}
};