#pragma once
#include "..\stdafx.h"
extern BYTE *pMem;
extern HBITMAP hBit;

#define GRAPHICS Graphics::getInstance()

class Graphics : public Singleton<Graphics>
{
public:
	// 비트맵파일 관련
	BOOL LoadBitFile(BITMAPFILE* bitMapFile, const CHAR* fileName);						// 비트맵 파일을 불러오는 함수
	BOOL DrawBitFile(BITMAPFILE* bitMapFile, const UINT X, UINT Y);						// 비트맵 파일을 메모리에 옮기는 함수
	BOOL DeleteBitFile(BITMAPFILE* bitMapFile);											// 비트맵 파일을 제거하는 함수	
	BOOL TransparentBitFile(BITMAPFILE* bitMapFile, UINT color);						// 특정 색의 알파를 255로 만들어주는 함수
	BOOL AlphaBlending(BITMAPFILE* bitMapFile, UINT X, UINT Y);

	// 메모리 초기화
	BOOL MemoryClear(BOOL isWhite);

	// 간단한 그리기 도구
	BOOL DrawRectangle(UINT X, UINT Y, UINT width, UINT height, MYBRUSH* pBrush);		// 브러쉬에서 저장하고 있는 색상값을 이용해서 사각형을 그리는 함수
	UINT MyRGB(COLORREF RGB);												// RGB 값을 만드는 함수
	MYBRUSH* CreateBrush(UINT color);													// RGB 값을 받아서 브러쉬를 생성하는 함수
	BOOL DeleteBrush(MYBRUSH* brush);													// 브러쉬를 삭제하는 함수
	
	// 메모리를 비트맵으로 만드는 함수
	BOOL Finish(VOID);																	// 메모리에 있는 값을 비트맵으로 만드는 과정
};