#include "stdafx.h"
#include "Graphics.h"

// 비트맵파일 관련
BOOL Graphics::LoadBitFile(BITMAPFILE* bitMapFile, const CHAR* fileName)
{
	if (bitMapFile == nullptr || fileName == nullptr)
		return FALSE;

	FILE *fp;
	fopen_s(&fp, fileName, "rb");

	if (fp == nullptr)
		return FALSE;

	fread(&bitMapFile->bitFileHeader, sizeof(BITFILEHEADER), 1, fp);

	if (bitMapFile->bitFileHeader.bfType != 0x4D42)
	{
		fclose(fp);
		return FALSE;
	}

	fread(&bitMapFile->bitFileInfoHeader, sizeof(BITFILEINFOHEADER), 1, fp);

	fseek(fp, bitMapFile->bitFileHeader.bfOffBits, SEEK_SET);

	bitMapFile->buffer = new BYTE[bitMapFile->bitFileInfoHeader.biHeight * bitMapFile->bitFileInfoHeader.biWidth * 4];

	fread(bitMapFile->buffer, bitMapFile->bitFileInfoHeader.biHeight * bitMapFile->bitFileInfoHeader.biWidth * 4, 1, fp);

	fclose(fp);

	return true;
}
BOOL Graphics::DrawBitFile(BITMAPFILE* bitMapFile, const UINT X, const UINT Y)
{
	if (bitMapFile == nullptr)
		return FALSE;

	INT width = bitMapFile->bitFileInfoHeader.biWidth;
	INT height = bitMapFile->bitFileInfoHeader.biHeight;

	INT index = height - 1;
	
	for (INT y = Y; y < static_cast<INT>(Y + height); y++)
	{
		memcpy(pMem + y * WIN_WIDTH * 4 + (X * 4), bitMapFile->buffer + index * width * 4, width * 4);
		index--;
	}

	return true;
}
BOOL Graphics::DeleteBitFile(BITMAPFILE *bitMapFile) {
	
	if (bitMapFile == nullptr)
		return FALSE;

	delete[] bitMapFile->buffer;

	return TRUE;
}
BOOL Graphics::TransparentBitFile(BITMAPFILE *bitMapFile, UINT color)
{
	INT height = bitMapFile->bitFileInfoHeader.biHeight;
	INT width = bitMapFile->bitFileInfoHeader.biWidth;

	UINT buffer = 0;

	for (INT y = 0; y < height; y++)
	{
		for (INT x = 0; x < width; x++)
		{
			memcpy(&buffer, bitMapFile->buffer + y * width * 4 + (x * 4), 3);

			if (buffer == color)
			{
				*(bitMapFile->buffer + y * width * 4 + (x * 4) + 3) = 255;
			}
		}
	}

	return TRUE;
}
BOOL Graphics::AlphaBlending(BITMAPFILE *bitMapFile, UINT X, UINT Y)
{
	UINT width = bitMapFile->bitFileInfoHeader.biWidth;
	UINT height = bitMapFile->bitFileInfoHeader.biHeight;

	INT index = height - 1;

	FLOAT alpha = 0.0f;
	BYTE temp = 0;

	for (INT y = Y; y < static_cast<INT>(Y + height); y++)
	{
		if (y < 0 || y >= WIN_HEIGHT)
		{
			index--;
			continue;
		}
		for (INT x = X; x < static_cast<INT>(X + width); x++)
		{
			if (x < 0 || x >= WIN_WIDTH)
			{
				continue;
			}

			memcpy(&temp, bitMapFile->buffer + index * width * 4 + ((x - X) * 4) + 3, 1);
			alpha = static_cast<FLOAT>(temp / 255.0f);

			// R 속성
			pMem[y * WIN_WIDTH * 4 + x * 4 + 0] = (pMem[y * WIN_WIDTH * 4 + x * 4 + 0] * alpha) + (bitMapFile->buffer[index * width * 4 + ((x - X) * 4) + 0] * (1.0 - alpha));
			// G 속성
			pMem[y * WIN_WIDTH * 4 + x * 4 + 1] = (pMem[y * WIN_WIDTH * 4 + x * 4 + 1] * alpha) + (bitMapFile->buffer[index * width * 4 + ((x - X) * 4) + 1] * (1.0 - alpha));
			// B 속성
			pMem[y * WIN_WIDTH * 4 + x * 4 + 2] = (pMem[y * WIN_WIDTH * 4 + x * 4 + 2] * alpha) + (bitMapFile->buffer[index * width * 4 + ((x - X) * 4) + 2] * (1.0 - alpha));

		}

		index--;
	}

	return TRUE;
}

// 간단한 그리기 관련
BOOL Graphics::MemoryClear(BOOL isWhite)
{
	if (isWhite)
		memset(pMem, 255, WIN_WIDTH * WIN_HEIGHT * 4);
	else
		memset(pMem, 0, WIN_WIDTH * WIN_HEIGHT * 4);

	return true;
}
BOOL Graphics::DrawRectangle(UINT X, UINT Y, UINT width, UINT height, MYBRUSH* pBrush)
{
	for (INT y = Y; y < static_cast<INT>(Y + height); y++)
		memcpy(pMem + y * WIN_WIDTH * 4 + (X * 4), pBrush->DATA, 4 * width);

	return true;
}
UINT Graphics::MyRGB(COLORREF RGB)
{
	union {
		UINT result;
		BYTE ARGB[4];
	};

	// 3~0 순서대로 A,R,G,B
	ARGB[3] = 0;
	ARGB[2] = static_cast<BYTE>(RGB);
	ARGB[1] = static_cast<BYTE>(RGB >> 8);
	ARGB[0] = static_cast<BYTE>(RGB >> 16);

	return result;
}
MYBRUSH* Graphics::CreateBrush(UINT color)
{
	MYBRUSH *pBrush = nullptr;

	pBrush = new MYBRUSH;

	if (pBrush == nullptr)
		return nullptr;

	for (int x = 0; x < WIN_WIDTH; x++)
		memcpy(pBrush->DATA + x * 4, &color, 4);

	return pBrush;
}
BOOL Graphics::DeleteBrush(MYBRUSH* pbrush)
{
	if (pbrush == nullptr)
		return false;

	delete pbrush;
	return true;
}

// 메모리을 비트맵으로 바꾸는 함수
BOOL Graphics::Finish(VOID)
{
	SetBitmapBits(hBit, WIN_WIDTH * WIN_HEIGHT * 4, pMem);
	return true;
}