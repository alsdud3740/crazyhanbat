#include "stdafx.h"
#include "System.h"
#include ".\GameObject\ObjResource.h"

// 전역 변수
HDC hdc = nullptr;
HDC hMemDC = nullptr;
HBITMAP hBit = nullptr;
HBITMAP hOldBit = nullptr;
BYTE *pMem = nullptr;

// 문자열 함수
VOID MyAnsiToUnicode(CHAR* ansi, tstring* out)
{
	if (!ansi || !out)
		return;

	INT lenA = lstrlenA(ansi);
	INT lenW;
	WCHAR* alloc;

	lenW = ::MultiByteToWideChar(CP_ACP, 0, ansi, lenA, 0, 0);

	if (lenW > 0)
	{
		alloc = new WCHAR[lenW + 1];

		if (!alloc)
			return;

		::MultiByteToWideChar(CP_ACP, 0, ansi, lenA, alloc, lenW);
	}
	else
		return;

	alloc[lenW] = L'\0';

	out->append(alloc);

	delete[] alloc;
}
INT MyAnsiToInt(CHAR ansi)
{

	if ((INT)ansi > 57 && (INT)ansi < 48)
		return 0;

	return (INT)ansi - 48;
}
BOOL MyIntToAnsi(INT a, CHAR* buffer)
{
	if (a > 999)
		return 0;

	INT value = a, rest = a;
	INT i = 0;

	while (value != 0)
	{
		rest = value % 10;
		value /= 10;
	
		buffer[i] = rest + 48;
		i++;
	}

	buffer[i] = '\0';

	INT len = strlen(buffer);
	len--;
	i = 0;

	while (1)
	{
		CHAR temp2 = buffer[i];
		buffer[i] = buffer[len];
		buffer[len] = temp2;
		
		i++; len--;

		if (i == len)
			break;

		if (i > len)
			break;
	}

	return TRUE;
}

// 함수 모듈
VOID InitMemory(HWND hWnd);
VOID InitBlock(HWND hWnd);
VOID InitItem(HWND hWnd);
VOID InitMap(HWND hWnd);
VOID InitCharacter(HWND hWnd);
VOID InitEtc(HWND hWnd);

// Main Function
VOID Init(HWND hWnd)
{
	InitMemory(hWnd);
	InitBlock(hWnd);
	InitItem(hWnd);
	InitMap(hWnd);
	InitCharacter(hWnd);
	InitEtc(hWnd);
}

// -----------------------------------------------
VOID InitMemory(HWND hWnd)
{
	hdc = GetDC(hWnd);
	hMemDC = CreateCompatibleDC(hdc);
	hBit = CreateCompatibleBitmap(hdc, WIN_WIDTH, WIN_HEIGHT);
	hOldBit = reinterpret_cast<HBITMAP>(SelectObject(hMemDC, hBit));
	pMem = new BYTE[WIN_WIDTH * WIN_HEIGHT * 4];
	ZeroMemory(pMem, WIN_WIDTH * WIN_HEIGHT * 4);
}
VOID InitBlock(HWND hWnd)
{
	FILE* fp;
	array<CHAR, SIZE_128> buffer;
	array<CHAR, SIZE_32> path;
	array<CHAR, SIZE_32> name;
	array<CHAR, SIZE_8> temp;
	BOOL pass, wall;

	fopen_s(&fp, ".\\Text\\BlockData.txt", "r");

	if (!fp)
	{
		return;
	}
	
	
	
	while (fgets(buffer.data(), buffer.max_size(), fp) != NULL)
	{
		INT i = 0, j = 0;
		
		while (buffer[i] != '#')
		{
			path[j] = buffer[i];
			i++;
			j++;
		}

		path[j] = '\0';
		i++;
		j = 0;

		while (buffer[i] != '#')
		{
			name[j] = buffer[i];
			i++;
			j++;
		}

		name[j] = '\0';
		i++;
		j = 0;

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++;
			j++;
		}

		temp[j] = '\0';
		i++;
		j = 0;

		if (temp[j] == '0')
			wall = FALSE;
		else
			wall = TRUE;

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++;
			j++;
		}
		temp[j] = '\0';
		j = 0;

		if (temp[j] == '0')
			pass = FALSE;
		else
			pass = TRUE;

		BITMAPFILE bitFile;
		GRAPHICS->LoadBitFile(&bitFile, path.data());
		tstring wstr;

		MyAnsiToUnicode(name.data(), &wstr);

		Block* block = new Block(wall, pass, wstr.c_str(), bitFile);

		Resource->Insert(wstr.c_str(), block);
	}

	fclose(fp);
}
VOID InitItem(HWND hWnd)
{
	FILE* fp = nullptr;

	fopen_s(&fp, ".\\Text\\ItemData.txt", "r");

	if (fp == nullptr)
	{
		return;
	}
		
	array<CHAR, SIZE_16> name;
	array<CHAR, SIZE_64> path;
	INT speed = 0;
	INT length = 0;
	INT number = 0;

	while (!feof(fp))
	{
		fscanf_s(fp, "%s", name.data(), name.max_size());
		fscanf_s(fp, "%s", path.data(), path.max_size());
		fscanf_s(fp, "%d %d %d", &speed, &length, &number);

		printf("%s %s %d %d %d\n", name.data(), path.data(), speed, length, number);
		
		tstring tstr;
		MyAnsiToUnicode(name.data(), &tstr);

		BITMAPFILE bitFile;
		GRAPHICS->LoadBitFile(&bitFile, path.data());
		GRAPHICS->TransparentBitFile(&bitFile, GRAPHICS->MyRGB(RGB(255, 255, 255)));

		Item *item = new Item(speed, length, number, tstr.c_str(), bitFile);
		Resource->Insert(tstr.c_str(),item);
	}

	fclose(fp);
}
VOID InitMap(HWND hWnd)
{
	FILE* fp;
	array<CHAR, SIZE_16> name;
	array<CHAR, SIZE_128> path;
	array<CHAR, SIZE_128> temp;
	
	array<CHAR, SIZE_16> block;
	BITMAPFILE bitFile;

	fopen_s(&fp, ".\\Text\\MapData.txt", "r");

	if (!fp)
	{
		return;
	}

	while (fgets(name.data(), name.max_size(), fp) != NULL)
	{

		INT len = 0;
		BOOL flag = TRUE;

		flag = TRUE;

		MAP* newMap = new MAP;

		len = strlen(name.data());
		name[len - 1] = '\0';

		fgets(path.data(), path.max_size(), fp);

		len = strlen(path.data());
		path[len - 1] = '\0';
		GRAPHICS->LoadBitFile(&bitFile, path.data());

		newMap->mapBitFile = bitFile;

		while (flag)
		{
			fgets(block.data(), block.max_size(), fp);

			len = strlen(block.data());
			block[len - 1] = '\0';

			fgets(temp.data(), temp.max_size(), fp);

			int i = 0, x = 0, y = 0;

			while (1)
			{
				BOOL createFlag = FALSE;

				if (temp[i] == '#')
					i++;
				else if (temp[i] == 'N')
					break;
				else if (temp[i] == 'E')
				{
					flag = 0;

					tstring tstr;
					MyAnsiToUnicode(name.data(), &tstr);

					Resource->Insert(tstr.c_str(), newMap);

					break;
				}
				else
				{
					x = MyAnsiToInt(temp[i]);
					i++;
					y = MyAnsiToInt(temp[i]);
					i++;
					createFlag = TRUE;
				}
				

				if (createFlag == TRUE)
				{
					tstring tstr;
					MyAnsiToUnicode(block.data(), &tstr);

					Block* obj = reinterpret_cast<Block*>(Resource->FindObj(tstr.c_str()));

					Block* tempblock = new Block(*obj);
					tempblock->setPoint(x, y);

					newMap->blockList.emplace_front(tempblock);

					createFlag = FALSE;
				}
			}
		}
	}

	fclose(fp);
}
VOID InitCharacter(HWND hWnd)
{
	FILE* fp;
	array<CHAR, SIZE_256> buffer;
	array<CHAR, SIZE_16> name;
	array<CHAR, SIZE_32> path;
	array<CHAR, SIZE_8> temp;
	array<CHAR, SIZE_64> path2;
	array<CHAR, SIZE_64> path3;
	array<CHAR, SIZE_64> path4;

	INT initnumber = 0;
	INT initlength = 0;
	INT initspeed = 0;
	INT maxnumber = 0;
	INT maxlength = 0;
	INT maxspeed = 0;
	BITMAPFILE BitFile;

	fopen_s(&fp, ".\\Text\\CharacterData.txt", "r");

	if (!fp)
	{
		return;
	}

	while (fgets(buffer.data(), buffer.max_size(), fp) != NULL)
	{

		INT i = 0, j = 0;

		while (buffer[i] != '#')
		{
			name[j] = buffer[i];
			i++;
			j++;
		}
		
		name[j] = '\0';
		j = 0;
		i++;

		while (buffer[i] != '#')
		{
			path[j] = buffer[i];
			i++;
			j++;
		}

		path[j] = '\0';
		j = 0;
		i++;

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		initnumber = MyAnsiToInt(temp[j]);
		
		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		initlength = MyAnsiToInt(temp[j]);

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		initspeed = MyAnsiToInt(temp[j]);

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		maxnumber = MyAnsiToInt(temp[j]);

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		maxlength = MyAnsiToInt(temp[j]);

		while (buffer[i] != '#')
		{
			temp[j] = buffer[i];
			i++; j++;
		}

		temp[j] = '\0';
		j = 0;
		i++;

		while (buffer[i] != '#')
		{
			path2[j] = buffer[i];
			i++; j++;
		}

		path2[j] = '\0';
		j = 0;
		i++;

		while (buffer[i] != '#')
		{
			path3[j] = buffer[i];
			i++; j++;
		}

		path3[j] = '\0';
		j = 0;
		i++;

		while (buffer[i] != '#')
		{
			path4[j] = buffer[i];
			i++; j++;
		}

		path4[j] = '\0';
		j = 0;
		i++;

		maxspeed = MyAnsiToInt(temp[j]);

		Character* temp2 = new Character;

		GRAPHICS->LoadBitFile(&BitFile, path.data());
		GRAPHICS->TransparentBitFile(&BitFile, GRAPHICS->MyRGB(RGB(255, 255, 255)));

		BITMAPFILE bitFile2;
		GRAPHICS->LoadBitFile(&bitFile2, path2.data());
		GRAPHICS->TransparentBitFile(&bitFile2, GRAPHICS->MyRGB(RGB(255, 255, 255)));
		
		BITMAPFILE bitFile3;
		GRAPHICS->LoadBitFile(&bitFile3, path3.data());
		GRAPHICS->TransparentBitFile(&bitFile3, GRAPHICS->MyRGB(RGB(255, 255, 255)));

		BITMAPFILE bitFile4;
		GRAPHICS->LoadBitFile(&bitFile4, path4.data());
		GRAPHICS->TransparentBitFile(&bitFile4, GRAPHICS->MyRGB(RGB(255, 255, 255)));

		temp2->bitFile = BitFile;
		temp2->left = bitFile2;
		temp2->right = bitFile3;
		temp2->up = bitFile4;
		
		tstring tstr;
		MyAnsiToUnicode(name.data(), &tstr);

		temp2->characterName = tstr;
		temp2->initBoomLength = initlength;
		temp2->initBoomNumber = initnumber;
		temp2->initSpeed = initspeed;
		temp2->maxBoomLength = maxlength;
		temp2->maxBoomNumber = maxnumber;
		temp2->maxSpeed = maxspeed;

		Resource->Insert(tstr.c_str(), temp2);
	}

	fclose(fp);
}
VOID InitEtc(HWND hWnd)
{
	BITMAPFILE *bitFile = new BITMAPFILE;
	BITMAPFILE *bitFile2 = new BITMAPFILE;
	BITMAPFILE *bitFile3 = new BITMAPFILE;
	BITMAPFILE *bitFile4 = new BITMAPFILE;
	BITMAPFILE *bitFile5 = new BITMAPFILE;
	BITMAPFILE *bitFile6 = new BITMAPFILE;
	BITMAPFILE *bitFile7 = new BITMAPFILE;
	BITMAPFILE *bitFile8 = new BITMAPFILE;
	BITMAPFILE *bitFile9 = new BITMAPFILE;
	BITMAPFILE *bitFile10 = new BITMAPFILE;
	BITMAPFILE *bitFile11 = new BITMAPFILE;

	if (bitFile == 0)
		return;

	if (bitFile2 == 0)
		return;

	if (bitFile3 == 0)
		return;

	if (bitFile4 == 0)
		return;

	if (bitFile5 == 0)
		return;

	if (bitFile6 == 0)
		return;

	if (bitFile7 == 0)
		return;

	if (bitFile8 == 0)
		return;

	if (bitFile9 == 0)
		return;

	if (bitFile10 == 0)
		return;

	if (bitFile11 == 0)
		return;

	GRAPHICS->LoadBitFile(bitFile, ".\\Image\\Title\\title_00.bmp");
	Resource->Insert(L"Title", bitFile);

	GRAPHICS->LoadBitFile(bitFile2, ".\\Image\\Lobby\\Lobby.bmp");
	Resource->Insert(L"Lobby", bitFile2);

	GRAPHICS->LoadBitFile(bitFile3, ".\\Image\\UI\\StartButton.bmp");
	Resource->Insert(L"StartButton", bitFile3);

	GRAPHICS->LoadBitFile(bitFile4, ".\\Image\\UI\\select_ch1.bmp");
	Resource->Insert(L"SelectBase", bitFile4);

	GRAPHICS->LoadBitFile(bitFile5, ".\\Image\\UI\\ArrowButtonLeft.bmp");
	Resource->Insert(L"ArrowLeft", bitFile5);

	GRAPHICS->LoadBitFile(bitFile6, ".\\Image\\UI\\ArrowButtonRight.bmp");
	Resource->Insert(L"ArrowRight", bitFile6);

	GRAPHICS->LoadBitFile(bitFile7, ".\\Image\\Boom\\boom1.bmp");
	GRAPHICS->TransparentBitFile(bitFile7, GRAPHICS->MyRGB(RGB(255, 255, 255)));
	Resource->Insert(L"Boom1", bitFile7);

	GRAPHICS->LoadBitFile(bitFile8, ".\\Image\\Boom\\water_down.bmp");
	GRAPHICS->LoadBitFile(bitFile9, ".\\Image\\Boom\\water_left.bmp");
	GRAPHICS->LoadBitFile(bitFile11, ".\\Image\\Boom\\water_center.bmp");
	Resource->Insert(L"EffectDown", bitFile8);
	Resource->Insert(L"EffectLeft", bitFile9);
	Resource->Insert(L"EffectCenter", bitFile11);
	
	GRAPHICS->LoadBitFile(bitFile10, ".\\Image\\UI\\select_ch2.bmp");
	Resource->Insert(L"SelectSecond", bitFile10);

	Resource->Insert(L"GameBC", GRAPHICS->CreateBrush(GRAPHICS->MyRGB(RGB(13, 142, 223))));
}


