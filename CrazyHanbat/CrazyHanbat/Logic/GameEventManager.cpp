#include "stdafx.h"
#include "GameEventManager.h"

VOID GameEventManager::EventCheck()
{
	for (auto itr = this->boomList.begin(); itr != this->boomList.end();)
	{
		if (reinterpret_cast<Boom*>(*itr)->OnTime(this->deltaTime().count()))
		{
			reinterpret_cast<Boom*>(*itr)->OnBurst();
			itr = this->boomList.erase(itr);
		}
		else {
			itr++;
		}
	}
}

VOID GameEventManager::GameCheck()
{
	static INT TimeValue = 0;
	
	if (SCENEMANAGER->GetCurrentScene() == SCENE::GAME)
	{
		TimeValue += this->deltaTime().count();

		if (TimeValue >= 500)
		{
			printf("GameCheck %d\n", Resource->currentGame->playerList.size());
			if (Resource->currentGame->playerList.size() <= 1)
			{
				Player* player = Resource->currentGame->playerList.front();

				if (player != NULL)
				{
					printf("%d�� �̰��.\n", player->_id);
				}
				else
				{
					printf("���º�\n");
				}

				SCENEMANAGER->SetState(SCENE::Title);
				Resource->button.clear();
				Resource->currentLobby = new Lobby();
			}

			TimeValue -= 500;
		}
	}
}

VOID GameEventManager::CheckTime()
{
	newTime = NOW_TIMEPOINT;
	milliseconds mili = duration_cast<milliseconds>(newTime - oldTime);
	delta = duration_cast<milliseconds>(newTime - oldTime);
	oldTime = NOW_TIMEPOINT;
}

milliseconds GameEventManager::deltaTime()
{
	return delta;
}