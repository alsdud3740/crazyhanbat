#pragma once
#include "..\stdafx.h"
#include "..\Util\Singleton.h"

#define SCENEMANAGER		SceneManager::getInstance()

typedef enum : INT
{
	Title = 1,
	LOBBY,
	GAME,
}SCENE;

class SceneManager : public Singleton<SceneManager>
{
private:
	SCENE currentSceneState;
public:
	SceneManager() { this->currentSceneState = SCENE::Title; }
	VOID SetState(SCENE value);
	VOID GoNext();
	VOID GoPrev();
	SCENE GetCurrentScene();
};