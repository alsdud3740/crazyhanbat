#pragma once
#include "..\stdafx.h"
#include "..\Util\Singleton.h"

#define PLAYERMANAGER PlayerManager::getInstance()
#define PERPIXEL 200

class PlayerManager : public Singleton<PlayerManager>
{
private:
	Game* currentGame;
	FLOAT temp = 0.0f;
public:
	BOOL Up(INT _id);
	BOOL Down(INT _id);
	BOOL Right(INT _id);
	BOOL Left(INT _id);
	VOID Set(Game* game);
	BOOL OnAttack(INT _id);

private:
	VOID Check(Player* player);
};