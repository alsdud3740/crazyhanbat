#pragma once
#include "..\stdafx.h"
#include "..\GameObject\Boom.h"
#include "..\Util\Clock.h"
#include "..\Util\Singleton.h"
#include "..\GameObject\Boom.h"

#include <list>

#define EVENTMANAGER GameEventManager::getInstance()

class GameEventManager : public Singleton<GameEventManager>
{
private:
	timePoint oldTime = NOW_TIMEPOINT;
	timePoint newTime = NOW_TIMEPOINT;
	milliseconds delta;

public:
	list<Boom*> boomList;

public:
	VOID EventCheck();
	VOID CheckTime();
	VOID GameCheck();
	milliseconds deltaTime();
};