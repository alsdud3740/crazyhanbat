#include "stdafx.h"
#include "Scene.h"


VOID SceneManager::SetState(SCENE value)
{
	this->currentSceneState = value;
}

VOID SceneManager::GoNext()
{
	INT currentNum = static_cast<INT>(this->currentSceneState);
	currentNum++;
	this->currentSceneState = (SCENE)currentNum;
}

VOID SceneManager::GoPrev()
{
	INT currentNum = static_cast<INT>(this->currentSceneState);
	currentNum--;
	this->currentSceneState = (SCENE)currentNum;
}

SCENE SceneManager::GetCurrentScene()
{
	return this->currentSceneState;
}