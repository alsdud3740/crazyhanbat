#include "stdafx.h"
#include "PlayerManager.h"

BOOL PlayerManager::Up(INT _id)
{
	Player* player = nullptr;

	for (auto& i : this->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}

	if (player == nullptr)
		return FALSE;

	player->direction = DIRECTION::Up;

	POINT point = player->getPoint();

	INT currentY = point.y % 40;
	INT indexY = point.y / 40;

	// 가장 위 쪽에 있는 벽에 있을 경우
	if (indexY == 0 && currentY <= 20)
	{
		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else if (indexY == 0 && currentY > 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if ((point.y / 40) < 0)
			point.y = 20;
		else if ((point.x % 40) < 25)
			point.y = 20;

		player->setPoint(point.x, point.y);

		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}

	// 평범한 경우
	if (this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].empty())
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		player->setPoint(point.x, point.y);

		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else if (this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].front()->type == OBJTYPE::tPlayer || this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].front()->type == OBJTYPE::tItem)
	{
		if (this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].size() == 1)
		{
			temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

			if (static_cast<INT>(temp) >= 1)
			{
				point.y -= static_cast<INT>(temp);
				temp -= static_cast<INT>(temp);
			}

			player->setPoint(point.x, point.y);

			if (indexY != (player->getPoint().y / 40))
				Check(player);

			return TRUE;
		}
		else if (this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].size() == 2)
		{
			auto& itr = this->currentGame->GameObject[(indexY - 1) * 15 + (point.x / 40)].begin();
			itr++;

			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer || reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			{
				temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

				if (static_cast<INT>(temp) >= 1)
				{
					point.y -= static_cast<INT>(temp);
					temp -= static_cast<INT>(temp);
				}

				player->setPoint(point.x, point.y);

				if (indexY != (player->getPoint().y / 40))
					Check(player);

				return TRUE;
			}
		}
	}
	else if (currentY > 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if (point.y % 40 < 20)
			point.y = indexY * 40 + 20;

		player->setPoint(point.x, point.y);

		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else {
		return TRUE;
	}

	return FALSE;
}

BOOL PlayerManager::Down(INT _id)
{
	Player* player = nullptr;

	for (auto& i : this->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}

	if (player == nullptr)
		return FALSE;

	player->direction = DIRECTION::Down;

	POINT point = player->getPoint();

	INT currentY = point.y % 40;
	INT indexY = point.y / 40;

	// 밑 가장 아래쪽 벽에 있을 경우
	if (indexY == 12 && currentY >= 20)
	{
		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else if (indexY == 12 && currentY <= 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if ((point.y / 40) >= 13)
			point.y = 500;
		else if ((point.x % 40) > 25)
			point.y = 500;

		player->setPoint(point.x, point.y);

		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}

	// 평범한 경우
	if (this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].empty())
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		player->setPoint(point.x, point.y);
	
		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else if (this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].front()->type == OBJTYPE::tPlayer || this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].front()->type == OBJTYPE::tItem)
	{
		if (this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].size() == 1)
		{
			temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

			if (static_cast<INT>(temp) >= 1)
			{
				point.y += static_cast<INT>(temp);
				temp -= static_cast<INT>(temp);
			}

			player->setPoint(point.x, point.y);

			if (indexY != (player->getPoint().y / 40))
				Check(player);

			return TRUE;
		}
		else if (this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].size() == 2)
		{
			auto& itr = this->currentGame->GameObject[(indexY + 1) * 15 + (point.x / 40)].begin();
			itr++;

			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer || reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			{
				temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

				if (static_cast<INT>(temp) >= 1)
				{
					point.y += static_cast<INT>(temp);
					temp -= static_cast<INT>(temp);
				}

				player->setPoint(point.x, point.y);

				if (indexY != (player->getPoint().y / 40))
					Check(player);

				return TRUE;
			}
		}
	}
	else if (currentY < 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.y += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if (point.y % 40 > 20)
			point.y = indexY * 40 + 20;

		player->setPoint(point.x, point.y);

		if (indexY != (player->getPoint().y / 40))
			Check(player);

		return TRUE;
	}
	else {
		return TRUE;
	}

	return FALSE;
}

BOOL PlayerManager::Right(INT _id)
{
	Player* player = nullptr;

	for (auto& i : this->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}
	
	if (player == nullptr)
		return FALSE;

	player->direction = DIRECTION::Right;

	POINT point = player->getPoint();

	INT currentX = point.x % 40;
	INT indexX = point.x / 40;

	// 오른쪽 마지막 벽에 있을 경우
	if(indexX == 14 && currentX >= 20)
	{
		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else if (indexX == 14 && currentX <= 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.x += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if ((point.x / 40) >= 15)
			point.x = 560;
		else if ((point.x % 40) > 25)
			point.x = 560;

		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}

	// 평범한 경우
	if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].empty())
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());
		
		if (static_cast<INT>(temp) >= 1)
		{
			point.x += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}
		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].front()->type == OBJTYPE::tPlayer || this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].front()->type == OBJTYPE::tItem)
	{
		if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].size() == 1)
		{
			temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

			if (static_cast<INT>(temp) >= 1)
			{
				point.x += static_cast<INT>(temp);
				temp -= static_cast<INT>(temp);
			}
			player->setPoint(point.x, point.y);

			if (indexX != (player->getPoint().x / 40))
				Check(player);

			return TRUE;
		}
		else if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].size() == 2)
		{
			auto& itr = this->currentGame->GameObject[(point.y / 40) * 15 + indexX + 1].begin();
			itr++;

			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer || reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			{
				temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

				if (static_cast<INT>(temp) >= 1)
				{
					point.x += static_cast<INT>(temp);
					temp -= static_cast<INT>(temp);
				}
				player->setPoint(point.x, point.y);

				if (indexX != (player->getPoint().x / 40))
					Check(player);

				return TRUE;
			}
		}
	}
	else if(currentX < 20)	// 옆이 비어있지 않고, Player가 아닌 경우..
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.x += static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if (point.x % 40 > 20)
			point.x = indexX * 40 + 20;

		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else
	{
		return TRUE;
	}


	return FALSE;
}

BOOL PlayerManager::Left(INT _id)
{
	Player* player = nullptr;

	for (auto& i : this->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}

	if (player == nullptr)
		return FALSE;

	player->direction = DIRECTION::Left;

	POINT point = player->getPoint();

	INT currentX = point.x % 40;
	INT indexX = point.x / 40;

	// 왼쪽 마지막 벽에 있을 경우
	if (indexX == 0 && currentX <= 20)
	{
		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else if (indexX == 0 && currentX >= 20)
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.x -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if ((point.x / 40) < 0)
			point.x = 20;
		else if ((point.x % 40) < 20)
			point.x = 20;

		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}

	// 평범한 경우
	if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].empty())
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.x -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}
		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].front()->type == OBJTYPE::tPlayer || this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].front()->type == OBJTYPE::tItem)
	{
		if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].size() == 1)
		{
			temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

			if (static_cast<INT>(temp) >= 1)
			{
				point.x -= static_cast<INT>(temp);
				temp -= static_cast<INT>(temp);
			}
			player->setPoint(point.x, point.y);

			if (indexX != (player->getPoint().x / 40))
				Check(player);

			return TRUE;
		}
		else if (this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].size() == 2)
		{
			auto& itr = this->currentGame->GameObject[(point.y / 40) * 15 + indexX - 1].begin();
			itr++;

			if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tPlayer || reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
			{
				temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

				if (static_cast<INT>(temp) >= 1)
				{
					point.x -= static_cast<INT>(temp);
					temp -= static_cast<INT>(temp);
				}
				player->setPoint(point.x, point.y);

				if (indexX != (player->getPoint().x / 40))
					Check(player);

				return TRUE;
			}
		}
	}
	else if (currentX > 20)	// 옆이 비어있지 않고, Player가 아닌 경우..
	{
		temp += static_cast<FLOAT>(PERPIXEL / 1000.f) * static_cast<FLOAT>(EVENTMANAGER->deltaTime().count());

		if (static_cast<INT>(temp) >= 1)
		{
			point.x -= static_cast<INT>(temp);
			temp -= static_cast<INT>(temp);
		}

		if (point.x % 40 < 20)
			point.x = indexX * 40 + 20;

		player->setPoint(point.x, point.y);

		if (indexX != (player->getPoint().x / 40))
			Check(player);

		return TRUE;
	}
	else
	{
		return TRUE;
	}

	return FALSE;
}

BOOL PlayerManager::OnAttack(INT _id)
{
	Player* player = nullptr;

	for (auto& i : this->currentGame->playerList)
	{
		if (i->_id == _id)
		{
			player = i;
		}
	}

	if (player == nullptr)
		return FALSE;

	POINT point = player->getPoint();

	INT indexX = point.x / 40;
	INT indexY = point.y / 40;

	for (auto& i : this->currentGame->GameObject[indexY * 15 + indexX])
	{
		if (i->type == OBJTYPE::tBoom)
		{
			return TRUE;
		}
	}

	player->OnAttack();
}

VOID PlayerManager::Set(Game* game)
{
	this->currentGame = game;
}

VOID PlayerManager::Check(Player* player)
{

	for (auto &i : this->currentGame->GameObject)
	{
		for (auto itr = i.begin(); itr != i.end();)
		{
			if (*itr == player)
			{
				itr = i.erase(itr);
			}
			else
				itr++;
		}
	}

	POINT point = player->getPoint();

	this->currentGame->GameObject[(point.y / 40) * 15 + (point.x / 40)].push_back(player);

	for (auto itr = this->currentGame->GameObject[(point.y / 40) * 15 + (point.x / 40)].begin(); itr != this->currentGame->GameObject[(point.y / 40) * 15 + (point.x / 40)].end();)
	{
		if (reinterpret_cast<Object*>(*itr)->type == OBJTYPE::tItem)
		{
			if (reinterpret_cast<Item*>(*itr)->flag == TRUE)
			{
				reinterpret_cast<Item*>(*itr)->Onget(player->_id);
				itr = this->currentGame->GameObject[(point.y / 40) * 15 + (point.x / 40)].erase(itr);
			}
		}
		else
			itr++;
	}
}