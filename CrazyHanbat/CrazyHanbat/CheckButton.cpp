#include "stdafx.h"
#include "System.h"

VOID CheckButton(HWND hWnd, INT x, INT y)
{
	if (SCENEMANAGER->GetCurrentScene() == SCENE::LOBBY)
	{
		for (auto i = Resource->button.begin(); i != Resource->button.end(); i++)
		{
			INT Width = reinterpret_cast<Button*>(*i)->x + reinterpret_cast<Button*>(*i)->width;
			INT Height = reinterpret_cast<Button*>(*i)->y + reinterpret_cast<Button*>(*i)->height;

			if (x >= reinterpret_cast<Button*>(*i)->x && x <= Width && y >= reinterpret_cast<Button*>(*i)->y && y <= Height)
			{
				reinterpret_cast<Button*>(*i)->OnClick();
			}
		}
	}
}